var express = require('express');
var mongo = require('mongodb');
var assert = require('assert');
var bodyParser = require('body-parser');
var cors = require('cors');
var ObjectID = require('mongodb').ObjectID;

var app = express();

//Routes


// Configuring Ports

app.set('port', (process.env.PORT || 8000));
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.listen(app.get('port'), function () {
    console.log('Node app is running on port', app.get('port'));
});

//APP ON THE LOCAL MACHINE
var prod = true;
// var url = "mongodb://localhost:27017/nextstacks";

if (prod) {
    var prod_url = require('./configuration/connection');
    url = prod_url;
}

//Configuring Routes