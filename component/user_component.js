var secret_key = "6deb624110a002bc031d26ad4fae193826991dd0cfeff33677a439c8cf56630ee694d43ecba808c9d4d1d0c48bae0d54939c949c1826a9b1227060796a169454";

module.exports = function(mongo,ObjectId,url,assert,dbb){
    return {
        register:function(user_details,callback){
            try {
                mongo.connect(url,{useNewUrlParser:false},function(err,db){
                    assert.equal(null,err);

                    db.db().collection(dbb.User).insertOne(user_details,function(err, result){
                        if(err){
                            callback(null,true,"Error Occured");
                        }else{
                            callback(result,false,"User added");
                        }
                    })
                })
            } catch (error) {
                callback(null,true,error);
            }
        },
        findUser:function(login_details,callback){
            try{
                mongo.collection(url,{useNewUrlParser:false},function(err,db){
                    assert.equal(null,err);
                    db.db().collection(dbb.User).findOne({
                        "email":login_details['email'],
                        "password":login_details['password']
                    },function(err,result){
                        if(err){
                            callback(null,true,"Error Occured");
                        }else{
                            callback(result,false,"");
                        }
                    })
                })
            }catch (error) {
                callback(null,true,error);
            }
        }
    }
}